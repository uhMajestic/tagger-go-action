module tagger-go-action

go 1.20

require (
	code.gitea.io/sdk/gitea v0.15.1 // indirect
	github.com/hashicorp/go-version v1.6.0 // indirect
	github.com/sethvargo/go-envconfig v0.9.0 // indirect
	github.com/sethvargo/go-githubactions v1.1.0 // indirect
)
