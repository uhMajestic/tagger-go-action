# tagger-go-action

gitea go action to create tags.

My use case is to create a tag using a custom token so I can cascade an action that actually releases with the tagged version.

## Usage

```yaml
name: Tag release commit
on:
  push:
    branches:
      - 'release'

jobs:
  tag-release:
    runs-on: windows10_x64
    defaults:
      run:
        shell: pwsh
    steps:

      - name: setup go
        uses: https://github.com/actions/setup-go@v4
        with:
          go-version: '>=1.20'

      - name: checkout code
        uses: https://gitea.com/actions/checkout@v3
        with:
          fetch-depth: 0

      - name: tag commit
        uses: https://gitea.com/uhMajestic/tagger-go-action@main
        with:
          gitea-token: ${{ secrets.TAG_TOKEN }}
          tag: your-version-here
```

### Reference

- [Triggering a workflow from a workflow](https://docs.github.com/en/actions/using-workflows/triggering-a-workflow#triggering-a-workflow-from-a-workflow)
- [Creating go Actions](https://blog.gitea.io/2023/04/creating-go-actions/)
- [actions/release-action](https://gitea.com/actions/release-action/src/branch/main/main.go)