package main

import (
	"crypto/tls"
	"fmt"
	"net/http"
	"os"
	"strconv"
	"strings"

	"code.gitea.io/sdk/gitea"
	gha "github.com/sethvargo/go-githubactions"
)

func main() {
	ctx, err := gha.Context()
	if err != nil {
		gha.Fatalf("failed to get action context: %s", err)
	}

	giteaToken := gha.GetInput("gitea-token")
	tag := gha.GetInput("tag")
	if giteaToken == "" {
		giteaToken = os.Getenv("GITEA_TOKEN")
	}
	insecure, _ := strconv.ParseBool(gha.GetInput("insecure"))

	client := http.DefaultClient
	if insecure {
		tr := &http.Transport{
			TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
		}
		client = &http.Client{Transport: tr}
	}

	giteaClient, err := gitea.NewClient(ctx.ServerURL, gitea.SetToken(giteaToken), gitea.SetHTTPClient(client))
	if err != nil {
		gha.Fatalf("failed to create gitea client: %s", err)
	}

	owner := ctx.RepositoryOwner
	repo := strings.Split(ctx.Repository, "/")[1]

	fmt.Println("trying to tag commit: ", ctx.SHA)

	createdTag, response, err := giteaClient.CreateTag(owner, repo, gitea.CreateTagOption{
		TagName: tag,
		Target:  ctx.SHA,
	})

	if err != nil {
		gha.Fatalf("failed to create tag: %s", err)
	}

	if response.StatusCode > 299 {
		gha.Fatalf("failed with status code: %d and\nbody: %s\n", response.StatusCode, response.Body)
	}

	gha.SetOutput("tag", createdTag.Name)
}
